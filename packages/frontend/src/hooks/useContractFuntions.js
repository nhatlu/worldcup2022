import { useContractFunction } from "@usedapp/core";
import { useCallback, useEffect, useState } from "react";
import { toast } from "react-toastify";

const useContractFuntions = (actionArray) => {
  const actions = {};

  actionArray.forEach((action) => {
    const [state, func] = GetContractFunction(action.contract, action.func);

    actions[action.key ?? action.func] = {
      state: state,
      func: func,
    };
  });

  console.log("useContractFuntions", actions);

  return actions;
};

const useActionState = (actions) => {
  const [action, setAction] = useState("");
  const [lastAction, setLastAction] = useState("");
  const [success, setSuccess] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleState = useCallback(async (action) => {
    setLoading(true);
    setAction(action);
  }, []);

  useEffect(() => {
    setLastAction(action);
  }, [action]);

  useEffect(() => {
    if (action) {
      const state = actions[action]?.state;

      switch (state.status) {
        case "Success":
          setSuccess(true);
          setLoading(false);
          break;
        case "Mining":
          break;
        case "Exception":
          toast(state.errorMessage);
          setAction("");
          state.status = "";
          setLoading(false);
          break;
        default:
          break;
      }
    }
  }, [action, actions]);

  return [lastAction, success, handleState, loading];
};

const GetContractFunction = (contract, method) => {
  const { state, send } = useContractFunction(contract, method, {
    transactionName: method,
  });

  console.log("GetContractFunction", method, state);

  return [state, send];
};

export { useContractFuntions, useActionState };
