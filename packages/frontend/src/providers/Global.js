import { createContext, useContext } from "react";
import { addressEqual, useCalls, useEthers } from "@usedapp/core";
import { utils } from "ethers";
import { Contract } from "@ethersproject/contracts";
import abi from "data/abi.json";
import { formatEther } from "ethers/lib/utils";

const globalContext = createContext();

const ProvideGlobal = ({ children }) => {
  const wethInterface = new utils.Interface(abi);
  const wethContractAddress = process.env.REACT_APP_CONTRACT;
  const contract = new Contract(wethContractAddress, wethInterface);
  const { account } = useEthers();

  const provideValue = {
    globalLoading: true,
    isOwner: false,
    isOpen: false,
    isPicked: 0,
    isWhitelisted: false,
    contract: contract,
    users: {},
  };

  const calls = [
    {
      contract: contract,
      method: "owner",
    },
    {
      contract: contract,
      method: "isOpen",
    },
    {
      contract: contract,
      method: "isWhitelisted",
      args: [account],
    },

    {
      contract: contract,
      method: "getUsers",
    },
  ];

  const results = useCalls(calls) ?? [];

  results.forEach((result, idx) => {
    if (result) {
      switch (idx) {
        case 0:
          if (account && result?.value?.[0] === account) {
            provideValue.isOwner = true;
          }

          provideValue.globalLoading = false;

          break;
        case 1:
          provideValue.isOpen = result?.value?.[0];
          break;

        case 2:
          provideValue.isWhitelisted = result?.value?.[0];
          break;

        case 3:
          const users = result?.value?.[0]
            .map((user) => ({
              address: user._address,
              pickId: parseInt(formatEther(user.pickId)),
              picked: user.picked,
            }))
            // unique
            .filter(
              (v, i, a) => a.findIndex((v2) => v2.address === v.address) === i
            )
            // add null
            .filter(
              (v) =>
                v.picked === true &&
                !addressEqual(
                  v.address,
                  "0x0000000000000000000000000000000000000000"
                )
            );

          // map
          users.forEach((element) => {
            if (element.address === account && element.picked) {
              provideValue.isPicked = element.pickId;
            }

            if (provideValue.users[element.pickId]) {
              provideValue.users[element.pickId].push(element);
            } else {
              provideValue.users[element.pickId] = [element];
            }
          });

          break;
        default:
          break;
      }
    }
  });

  console.log("ProvideGlobal render", provideValue);

  return (
    <globalContext.Provider value={provideValue}>
      {children}
    </globalContext.Provider>
  );
};

const useGlobal = () => {
  return useContext(globalContext);
};

export { ProvideGlobal, useGlobal };
