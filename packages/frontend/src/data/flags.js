const flags = [
  {
    name: "Netherlands",
    flag: "nl",
    lose: true,
  },
  {
    name: "Argentina",
    flag: "ar",
    lose: false,
  },
  {
    name: "England",
    flag: "gb-eng",
    lose: true,
  },
  {
    name: "France",
    flag: "fr",
    lose: false,
  },
  {
    name: "Croatia",
    flag: "hr",
    lose: false,
  },
  {
    name: "Brazil",
    flag: "br",
    lose: true,
  },
  {
    name: "Morocco",
    flag: "vn",
    lose: false,
  },
  {
    name: "Portugal",
    flag: "pt",
    lose: true,
  },
];

export default flags;
