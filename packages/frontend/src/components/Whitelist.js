import { useGlobal } from "providers/Global";

import Form from "react-bootstrap/Form";

import { useState } from "react";
import { useContractFuntions, useActionState } from "hooks/useContractFuntions";
import Button from "components/Button";

export default function Whitelist() {
  const { isOwner, isOpen, contract } = useGlobal();
  const [whitelist, setWhitelist] = useState("");

  const actions = useContractFuntions([
    {
      contract: contract,
      func: "addList",
    },
    {
      contract: contract,
      func: "removeList",
    },

    {
      contract: contract,
      func: "setOpen",
    },
    {
      contract: contract,
      func: "setClose",
    },
    {
      contract: contract,
      func: "unpickbyOwner",
    },
  ]);

  const [lastAction, , handleState, loading] = useActionState(actions);

  const handleAction = (action) => {
    switch (action) {
      case "addList":
        console.log("whitelist", whitelist.split("\n"));
        actions[action].func(whitelist.split("\n"));

        break;

      case "removeList":
        actions[action].func(whitelist);

        break;
      case "setOpen":
        actions[action].func();

        break;

      case "setClose":
        actions[action].func();

        break;

      case "unpickbyOwner":
        actions[action].func(whitelist);

        break;

      default:
        break;
    }

    handleState(action);
  };

  return (
    isOwner && (
      <div className="container mt-5">
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Whitelist</Form.Label>
          <Form.Control
            as="textarea"
            style={{ height: "100px" }}
            defaultValue={whitelist}
            onChange={({ target: { value } }) => setWhitelist(value)}
          />
        </Form.Group>

        <Button
          action="addList"
          lastAction={lastAction}
          loading={loading}
          onClick={() => handleAction("addList")}
        >
          Add Whitelist
        </Button>
        <Button
          action="removeList"
          lastAction={lastAction}
          loading={loading}
          onClick={() => handleAction("removeList")}
          className="mx-1"
        >
          Remove Whitelist
        </Button>

        <Button
          action="unpickbyOwner"
          lastAction={lastAction}
          loading={loading}
          onClick={() => handleAction("unpickbyOwner")}
          className="mx-1"
        >
          unpickbyOwner
        </Button>

        {isOpen ? (
          <Button
            action="setClose"
            lastAction={lastAction}
            loading={loading}
            onClick={() => handleAction("setClose")}
            className="ms-1"
          >
            Close
          </Button>
        ) : (
          <Button
            action="setOpen"
            lastAction={lastAction}
            loading={loading}
            onClick={() => handleAction("setOpen")}
          >
            Open
          </Button>
        )}
      </div>
    )
  );
}
