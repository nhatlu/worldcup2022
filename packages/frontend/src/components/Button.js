import { Button as ButtonBT, Spinner } from "react-bootstrap";

export default function Button({
  children,
  action,
  lastAction,
  loading,
  ...props
}) {
  return (
    <ButtonBT {...props} variant="primary" type="buttton">
      {lastAction === action && loading ? (
        <Spinner
          as="span"
          animation="border"
          size="sm"
          role="status"
          aria-hidden="true"
        />
      ) : (
        <>{children}</>
      )}
    </ButtonBT>
  );
}
