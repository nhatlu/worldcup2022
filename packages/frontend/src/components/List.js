import flags from "data/flags";
import Table from "react-bootstrap/Table";
import { parseEther } from "@ethersproject/units";
import { useGlobal } from "providers/Global";
import { useActionState, useContractFuntions } from "hooks/useContractFuntions";
import ContentLoader from "react-content-loader";
import "flag-icons/css/flag-icons.min.css";
import { useEthers } from "@usedapp/core";

export default function List() {
  const { contract, users, isOpen, isPicked, isWhitelisted, globalLoading } =
    useGlobal();

  const { account } = useEthers();

  const actions = useContractFuntions([
    {
      contract: contract,
      func: "pick",
    },
    {
      contract: contract,
      func: "unpick",
    },
  ]);

  const [, , handleState, loading] = useActionState(actions);

  const handleAction = (id, action) => {
    if (action === "pick") {
      actions["pick"].func(parseEther(id));
      handleState("pick");
    } else {
      actions["unpick"].func();
      handleState("unpick");
    }
  };

  return (
    <div className="container mt-5">
      {!globalLoading &&
        (!account ? (
          <p>Please connect MetaMask wallet to vote</p>
        ) : !isWhitelisted ? (
          <>
            <p>Your wallet is not in whitelist. </p>
            <p>
              Please add your wallet in the link bellow:
              <br />
              https://docs.google.com/spreadsheets/d/1sTxbRuAhPt78lUDLUb6Yi9yEoEirHFm7XtE32dVlQZU/edit#gid=0
            </p>
          </>
        ) : (
          ""
        ))}
      <Table bordered hover responsive>
        <thead>
          <tr>
            <th className="col-md-1 text-center" scope="col">
              #
            </th>
            <th scope="col">Team</th>
            <th className="col-md-1 text-center" scope="col">
              Picked
            </th>
            <th className="col-md-1 text-center" scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {flags.map((flag, index) => (
            <tr
              key={index}
              className={
                isPicked === index + 1
                  ? "bg-success bg-opacity-25 fw-bold opacity-50"
                  : flag.lose
                  ? "opacity-25"
                  : ""
              }
            >
              <td className="py-1 align-middle text-center">{index + 1}</td>
              <td>
                <span
                  className={`fi fi-${flag.flag} me-3`}
                  style={{ width: "2rem", lineHeight: "2rem" }}
                ></span>
                {flag.name}
              </td>
              <td className="py-1 align-middle text-center">
                {globalLoading ? (
                  <ContentLoader
                    speed={2}
                    width={20}
                    height={20}
                    viewBox="0 0 50 50"
                  >
                    <circle cx="20" cy="20" r="20" />
                  </ContentLoader>
                ) : (
                  users[index + 1]?.length ?? 0
                )}
              </td>
              <td
                align="center"
                nowrap="nowrap"
                className="py-1 align-middle text-center"
              >
                {globalLoading ? (
                  <ContentLoader
                    speed={2}
                    width={100}
                    height={30}
                    viewBox="0 0 100 50"
                    backgroundColor="#f3f3f3"
                    foregroundColor="#ecebeb"
                  >
                    <rect x="15" y="12" rx="0" ry="0" width="248" height="79" />
                  </ContentLoader>
                ) : isOpen ? (
                  isPicked === 0 && isWhitelisted ? (
                    <>
                      {loading ? (
                        <div className="spinner-border" role="status">
                          <span className="visually-hidden">Loading...</span>
                        </div>
                      ) : (
                        <input
                          className="form-check-input  cursor-pointer"
                          type="checkbox"
                          onClick={() => handleAction(`${index + 1}`, "pick")}
                        />
                      )}
                    </>
                  ) : isPicked === index + 1 ? (
                    <>
                      {loading ? (
                        <div className="spinner-border" role="status">
                          <span className="visually-hidden">Loading...</span>
                        </div>
                      ) : (
                        <input
                          className="form-check-input cursor-pointer"
                          type="checkbox"
                          defaultChecked
                          onClick={() => handleAction(`${index + 1}`, "unpick")}
                        />
                      )}
                    </>
                  ) : (
                    ""
                  )
                ) : (
                  ""
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      <h6 className="mt-4">
        <strong>Time to vote:</strong> You can change your vote until the
        quanterfinal start
        <span className="text-danger ms-1">
          <strong>(09/12/2022)</strong>
        </span>
      </h6>
      <h5 className="mt-4">
        <strong>Winner:</strong>
        <span className="text-danger ms-2">
          <strong>2.000.000 VND</strong>
        </span>
      </h5>
      <p>The jackpot shall be equally shared to those who won the bet.</p>

      <h4 className="mt-5">
        <strong>Please add BNB Chain – Testnet network to MetaMask </strong>
      </h4>
      <p>
        <strong>Network name:</strong> BNB Chain – Testnet
      </p>
      <p>
        <strong>New RPC URL:</strong>
        https://data-seed-prebsc-1-s1.binance.org:8545
      </p>
      <p>
        <strong>Chain ID:</strong> 97
      </p>
      <p>
        <strong>Currency symbol:</strong> BNB
      </p>
      <p>
        <strong>Block explorer URL:</strong> https://testnet.bscscan.com
      </p>
      <p>
        <strong>Faucet testBNB at:</strong>
        https://testnet.bnbchain.org/faucet-smart
      </p>
    </div>
  );
}
