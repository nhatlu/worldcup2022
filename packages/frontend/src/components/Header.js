import { Button } from "react-bootstrap";
import { useEthers, shortenAddress } from "@usedapp/core";
import { BSCTestnet } from "@usedapp/core";

export default function Header() {
  const { activateBrowserWallet, account, switchNetwork, chainId } =
    useEthers();

  const handleConnect = async () => {
    if (chainId !== BSCTestnet.chainId) {
      await switchNetwork(BSCTestnet.chainId);
    }

    activateBrowserWallet();
  };

  return (
    <div
      className={`menu position-relative top-0 start-0 end-0 bg-white z-3 shadow-sm `}
    >
      <div className="container">
        <div className="menu-top d-flex justify-content-between py-3 align-items-center border-0 border-lg-bottom">
          <div className="menu-logo">
            <img src="/Logo.svg" alt="R Digital" />
            <img className="ms-5" src="/AesirX.svg" alt="AesirX" />
          </div>
          <div className="menu-button d-flex align-items-center align-items-lg-start">
            {account ? (
              <p>Account: {shortenAddress(account)}</p>
            ) : (
              <Button
                onClick={handleConnect}
                className="btn w-100 btn-success mt-2 text-uppercase rounded-4 font-opensans fw-bold text-white px-4 py-2 fs-10"
              >
                Connect
              </Button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
