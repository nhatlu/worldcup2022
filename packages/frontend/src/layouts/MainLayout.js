import Header from "components/Header";
import Spinner from "react-bootstrap/Spinner";
import { ProvideGlobal } from "providers/Global";
import React, { Suspense } from "react";

const List = React.lazy(() => import("components/List"));
const Notifications = React.lazy(() => import("components/Notifications"));
const Whitelist = React.lazy(() => import("components/Whitelist"));

export default function MainLayout() {
  console.log("MainLayout render", global);

  return (
    <ProvideGlobal>
      <Header />

      <Suspense
        fallback={
          <div class="d-flex justify-content-center mt-5">
            <Spinner animation="border" role="status">
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          </div>
        }
      >
        <List />
        <Whitelist />
        <Notifications />
      </Suspense>
    </ProvideGlobal>
  );
}
