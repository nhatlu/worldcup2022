import MainLayout from "layouts/MainLayout";
import "styles/App.scss";

import { BSCTestnet, DAppProvider } from "@usedapp/core";
import { getDefaultProvider } from "ethers";
import { ProvideGlobal } from "providers/Global";

const config = {
  readOnlyChainId: BSCTestnet.chainId,
  readOnlyUrls: {
    [BSCTestnet.chainId]: getDefaultProvider(
      "https://data-seed-prebsc-1-s3.binance.org:8545"
    ),
  },
  networks: [BSCTestnet],
};

function App() {
  return (
    <DAppProvider config={config}>
      <ProvideGlobal>
        <MainLayout />
      </ProvideGlobal>
    </DAppProvider>
  );
}

export default App;
