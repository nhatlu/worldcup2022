import { ethers, upgrades } from "hardhat";

async function main() {
  const proxyAddress = process.env.CONTRACT;
  const contractFactory = await ethers.getContractFactory("WorldCup");
  const token = await upgrades.upgradeProxy(proxyAddress, contractFactory);

  console.log("Upgrade Contract: ", token.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
