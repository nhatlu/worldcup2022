// contracts/MyContract.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "hardhat/console.sol";

contract WorldCup is Initializable, UUPSUpgradeable, OwnableUpgradeable {
    bool private hasOpen;
    mapping(address => bool) private whitelist;

    struct User {
        uint64 pickId;
        bool picked; // mark as picked
        address _address;
    }

    mapping(address => User) private users; // address => User => true/false;

    address[] totalUsers;

    function initialize() public initializer {
        __Ownable_init();
        hasOpen = false;
    }

    function _authorizeUpgrade(address) internal override onlyOwner {}

    function addList(address[] memory _addresses) external onlyOwner {
        for (uint256 i = 0; i < _addresses.length; i++) {
            require(whitelist[_addresses[i]] != true);
            whitelist[_addresses[i]] = true;
        }
    }

    function removeList(address _address) external onlyOwner {
        whitelist[_address] = false;
    }

    function isWhitelisted(address _address) public view returns (bool) {
        return whitelist[_address];
    }

    function setOpen() external onlyOwner {
        hasOpen = true;
    }

    function setClose() external onlyOwner {
        hasOpen = false;
    }

    function isOpen() public view returns (bool) {
        return hasOpen;
    }

    function getUsers() public view returns (User[] memory) {
        User[] memory _users = new User[](totalUsers.length);
        for (uint256 i = 0; i < totalUsers.length; i++) {
            _users[i] = users[totalUsers[i]];
        }
        return _users;
    }

    function pick(uint64 _pickId) external {
        require(hasOpen == true, "Not Open");

        address _sender = msg.sender;
        require(whitelist[_sender] == true, "Not Whitelist");

        User memory user = users[_sender];

        user.pickId = _pickId;
        user.picked = true;
        user._address = _sender;
        users[_sender] = user;
        totalUsers.push(_sender);
    }

    function unpick() external {
        require(hasOpen == true, "Not Open");

        address _sender = msg.sender;

        User memory user = users[_sender];
        user.picked = false;
        users[_sender] = user;
    }

    function isPicked() public view returns (bool) {
        address _sender = msg.sender;

        User memory user = users[_sender];

        return user.picked;
    }

    function unpickbyOwner(address _address) external onlyOwner {
        User memory user = users[_address];
        user.picked = false;
        users[_address] = user;
    }
}
