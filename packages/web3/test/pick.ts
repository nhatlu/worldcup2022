// @ts-ignore
import { utils } from "ethers";

import { expect } from "chai";
import { ethers, upgrades } from "hardhat";

describe("Pick", function () {
  let contract: any;
  let owner: any;
  let user1: any;
  let user2: any;

  const pe = (num: any) => utils.parseEther(num);

  beforeEach(async function () {
    [owner, user1, user2] = await ethers.getSigners();

    const worldCupContract = await ethers.getContractFactory("WorldCup");
    contract = await upgrades.deployProxy(worldCupContract, []);
  });

  it("Pick", async function () {
    let whitelist = [user1.address, user2.address];

    await contract.setOpen();
    await contract.addList(whitelist);
    await contract.connect(user1).pick(pe("1"));

    expect(await contract.connect(user1).isPicked()).to.equal(true);
  });

  it("UnPick", async function () {
    let whitelist = [user1.address, user2.address];

    await contract.setOpen();
    await contract.addList(whitelist);
    await contract.connect(user1).pick(pe("1"));
    await contract.connect(user2).pick(pe("2"));
    await contract.connect(user1).unpick();
    expect(await contract.connect(user1).isPicked()).to.equal(false);
    expect(await contract.connect(user2).isPicked()).to.equal(true);
  });

  it("RePick", async function () {
    let whitelist = [user1.address, user2.address];

    await contract.setOpen();
    await contract.addList(whitelist);
    await contract.connect(user1).pick(pe("1"));
    await contract.connect(user1).pick(pe("2"));

    expect(await contract.connect(user1).isPicked()).to.equal(true);
  });

  it("Not Whitelist", async function () {
    let whitelist = [user1.address];

    await contract.addList(whitelist);

    expect(contract.connect(user2).pick(pe("1"))).to.revertedWith(
      "Not Whitelist"
    );
  });
});
