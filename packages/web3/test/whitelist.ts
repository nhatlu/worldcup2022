import { expect } from "chai";
import { ethers, upgrades } from "hardhat";

describe("Whitelist", function () {
  let contract: any;
  let owner: any;
  let user1: any;
  let user2: any;

  beforeEach(async function () {
    [owner, user1, user2] = await ethers.getSigners();

    const worldCupContract = await ethers.getContractFactory("WorldCup");
    contract = await upgrades.deployProxy(worldCupContract, [], {
      kind: "uups",
    });
  });

  it("Add whitelist", async function () {
    let whitelist = [user1.address, user2.address];

    await contract.addList(whitelist);

    expect(await contract.isWhitelisted(user1.address)).to.equal(true);
    expect(await contract.isWhitelisted(user2.address)).to.equal(true);
  });

  it("Remove whitelist", async function () {
    let whitelist = [user1.address, user2.address];

    await contract.addList(whitelist);
    await contract.removeList(user1.address);

    expect(await contract.isWhitelisted(user1.address)).to.equal(false);
    expect(await contract.isWhitelisted(user2.address)).to.equal(true);
  });
});
